
NAME_CLIENT			:=			client
NAME_SERVER			:=			demon_server
SRC_DIR			:=			./srcs/
OBJ_DIR			:=			./obj/
INC_DIR			:=			./inc/
LIB_DIR			:=			./lib/

SRC_SERVER	:=			demon_server.c
SRC_CLIENT	:=			demon_server.c

OBJ_SERVER 			=			$(addprefix $(OBJ_DIR), $(SRC_SERVER:.c=.o))
OBJ_CLIENT 			=			$(addprefix $(OBJ_DIR), $(SRC_CLIENT:.c=.o))


HEADER_FLAGS	:=			-I $(INC_DIR)

CC				:=			gcc

all: $(NAME_SERVER) $(NAME_CLIENT)

$(NAME_SERVER): $(OBJ_SERVER)
	$(CC) $(OBJ_SERVER) -o $(NAME_SERVER)

$(OBJ_SERVER): | $(OBJ_DIR)
$(OBJ_CLIENT): | $(OBJ_DIR)

$(OBJ_DIR):
	 mkdir $(OBJ_DIR)

$(OBJ_DIR)%.o: %.c
	$(CC) -c $< -o $@ $(CC_FLAGS) $(HEADER_FLAGS)

clean:
	rm -f $(OBJ_SERVER)
	rm -f $(OBJ_CLIENT)

fclean: clean
	rm -f $(NAME_SERVER)
	rm -f $(NAME_CLIENT)
	rm -rf $(OBJ_DIR)

re: fclean all
vpath %.c $(SRC_DIR)
