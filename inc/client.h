
#ifndef CLIENT_H
# define CLIENT_H

#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <net/if.h>

#define BUFF_SIZE 1024

# define RED   "\x1B[31m"
# define RESET "\x1B[0m"

int   chaeck_meassgen(char *message);
int   check_interface(char *message, int sock);
int   check_stat(char *message);
int   check_show(char *message);
void  printf_name_of_interface(void);
void  baner(void);
void  about(void);
char* ReadFile(char *filename);
size_t substr_count(const char *str, const char* substr);
char	*ft_getline(void);
int		ft_getchar(void);

#endif
