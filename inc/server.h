
#ifndef SERVER_H
# define SERVER_H

#include <sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<netinet/ip_icmp.h>
#include<netinet/udp.h>
#include<netinet/tcp.h>
#include<netinet/ip.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<errno.h>
#include<netdb.h>
#include<netinet/ip6.h>
#include<netinet/if_ether.h>
#include<net/ethernet.h>
#include<sys/ioctl.h>
#include<sys/time.h>
#include<sys/types.h>
#include<unistd.h>
#include<time.h>
#include<signal.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>

#define PORT    "8888"
#define BACKLOG     10

int sock_raw;
FILE *logfile;
int tcp=0,udp=0,icmp=0,others=0,igmp=0,total=0,i,j;
struct sockaddr_in source,dest;
pid_t child_pid = -1;
pid_t parent_pid;

void process_packet(unsigned char* , int);
void print_ip_header(unsigned char* , int);
void print_tcp_packet(unsigned char* , int);
void print_udp_packet(unsigned char * , int);
void print_icmp_packet(unsigned char* , int);
int snifer(FILE *logfile, char *interface);
void handle(int newsock, fd_set *set);
int  	demon_server(void);
int   check_interface(char *message, int newsock);
void 	sigquit_handler (int sig);
void 	kill_child(int sig);

#endif
