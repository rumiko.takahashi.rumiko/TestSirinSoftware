#include "../inc/server.h"

int main(void)
{
  int status;
  int pid;

  if(geteuid() != 0)
  {
    printf("Start with sudo ./demon_server \n");
    return (0);
  }
  pid = fork();
  if (pid == -1)
    return (-1);
  else if (!pid)
  {
    umask(0);
    setsid();
    chdir("/");
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    while (1)
      status = demon_server();
    return status;
  }
  else
    return (0);
  return (0);
}

int  demon_server(void)
{
  int sock;
  struct addrinfo hints, *res;
  int reuseaddr = 1;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  if (getaddrinfo(NULL, PORT, &hints, &res) != 0) {
     perror("getaddrinfo");
     return 1;
  }
  sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
  if (sock == -1) {
     perror("socket");
     return 1;
  }
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int)) == -1) {
     perror("setsockopt");
     return 1;
  }
  if (bind(sock, res->ai_addr, res->ai_addrlen) == -1) {
     perror("bind");
     return 1;
  }
  if (listen(sock, BACKLOG) == -1) {
     perror("listen");
     return 1;
  }
  freeaddrinfo(res);
  while (1) {
     socklen_t size = sizeof(struct sockaddr_in);
     struct sockaddr_in their_addr;
     int newsock = accept(sock, (struct sockaddr*)&their_addr, &size);
     if (newsock == -1) {
         perror("accept");
     }
     else {
         printf("Got a connection from %s on port %d\n",
                 inet_ntoa(their_addr.sin_addr), htons(their_addr.sin_port));
         handle(newsock);
     }
 }
 close(sock);
 return 0;
}

int snifer(FILE *logfile, char *interface)
{
  int saddr_size , data_size;
  struct sockaddr saddr;
  struct in_addr in;
  char ip[255];
  unsigned char *buffer;

  signal(SIGKILL, kill_child);
  buffer = (unsigned char *)malloc(65536);
  if(logfile==NULL)
  {
    exit(0);
  }
  sock_raw = socket(AF_INET , SOCK_RAW , IPPROTO_TCP);
  if(sock_raw < 0)
  {
      return (1);
  }
  if (strcmp(interface, "all") != 0){
    if (setsockopt(sock_raw , SOL_SOCKET , SO_BINDTODEVICE ,interface , strlen(interface)+ 1 ) < 0)
    {
      return (0);
    }
  }
  while(1)
  {
    saddr_size = sizeof(saddr);
    data_size = recvfrom(sock_raw , buffer , 65536 , 0 , &saddr , &saddr_size);
    if(data_size <0)
    {
      return (1);
    }
    process_packet(buffer , data_size);
  }
  close(sock_raw);
  return (0);
}

void process_packet(unsigned char* buffer, int size)
{
    struct iphdr *iph = (struct iphdr*)buffer;
    ++total;
    fprintf(logfile,"   |-Type Protocol        : %s\n", iph->protocol);
    switch (iph->protocol)
    {
        case 1:
            ++icmp;
            // print_icmp_packet(buffer, size);
            break;

        case 2:
            ++igmp;
            break;

        case 6:
            ++tcp;
            // print_tcp_packet(buffer , size);
            break;

        case 17:
            ++udp;
            // print_udp_packet(buffer , size);
            break;

        default:
            ++others;
            break;
    }
}

void print_ip_header(unsigned char* Buffer, int Size)
{
    unsigned short iphdrlen;
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen =iph->ihl * 4;

    memset(&source, 0, sizeof(source));
    source.sin_addr.s_addr = iph->saddr;
    memset(&dest, 0, sizeof(dest));
    dest.sin_addr.s_addr = iph->daddr;
    fprintf(logfile,"   |-Source IP        : %s",inet_ntoa(source.sin_addr));
    fprintf(logfile,"   |-Destination IP   : %s",inet_ntoa(dest.sin_addr));
}

void print_tcp_packet(unsigned char* Buffer, int Size)
{
    unsigned short iphdrlen;
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen = iph->ihl*4;
    struct tcphdr *tcph=(struct tcphdr*)(Buffer + iphdrlen);
    fprintf(logfile,"TCP");
    fprintf(logfile, " %s ", asctime (timeinfo));
    print_ip_header(Buffer , Size);
    fprintf(logfile,"\n");
}

void print_udp_packet(unsigned char *Buffer , int Size)
{
    unsigned short iphdrlen;
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen = iph->ihl*4;
    struct udphdr *udph = (struct udphdr*)(Buffer + iphdrlen);
    fprintf(logfile,"UDP");
    fprintf(logfile, " %s ", asctime (timeinfo));
    print_ip_header(Buffer,Size);
    fprintf(logfile,"\n");
}

void print_icmp_packet(unsigned char* Buffer , int Size)
{
    unsigned short iphdrlen;
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen = iph->ihl*4;
    struct icmphdr *icmph = (struct icmphdr *)(Buffer + iphdrlen);
    fprintf(logfile,"ICMP");
    fprintf(logfile, " %s ", asctime (timeinfo));
    print_ip_header(Buffer , Size);
    fprintf(logfile,"\n");
}

void handle(int newsock)
{
  char buff[100];
  int   start;
  int status;

  start = 0;
  memset(buff, 0, 100);
  read(newsock, buff, 99);
  if (!strcmp(buff, "start"))
  {
    start = 1;
    write(newsock, "\nStart snifing\n", strlen("\nStart snifing\n"));
    child_pid = fork();
    if (child_pid == 0)
    {
      snifer(logfile, "all");
    }
  }
  if (strcmp(buff, "exit") == 0)
  {
    close(newsock);
    kill(child_pid, SIGKILL);
    kill(parent_pid, SIGKILL);
    exit(0);
  }
  else if (strcmp(buff, "stop") == 0)
  {
    kill(-parent_pid, SIGQUIT);
    for (;;)
    {
      pid_t child = wait(&status);
      if (child > 0 && WIFEXITED(status) && WEXITSTATUS(status) == 0)
        ;
      else if (child < 0 && errno == EINTR)
        continue;
      else
      {
        perror("wait");
        abort();
      }
      break;
    }
  }
  else if (check_interface(buff, newsock) == 1)
  {
    kill(-parent_pid, SIGQUIT);
    for (;;)
    {
      pid_t child = wait(&status);
      if (child > 0 && WIFEXITED(status) && WEXITSTATUS(status) == 0)
            ;
      else if (child < 0 && errno == EINTR)
        continue;
      else
      {
        perror("wait");
        abort();
      }
      break;
    }
    write(newsock, "\nChange interface snifing\n", strlen("\nChange interface snifing\n"));
    child_pid = fork();
    if (child_pid == 0)
      snifer(logfile, buff);
  }
  write(newsock, "All Good", strlen("All Good"));
}

int   check_interface(char *message, int newsock)
{
  int   family;
  int   flag;
  char  *interface;
  struct ifaddrs *ifa;
  struct ifaddrs *ifaddr;
  char ip[255];

  flag = 0;
  if (getifaddrs(&ifaddr) == -1)
    return (0);
  else
  {
    i = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
      if (ifa->ifa_addr == NULL)
        continue;
      family = ifa->ifa_addr->sa_family;
      if (family == AF_INET || family == AF_INET6)
      {
        if(!(ifa->ifa_flags & IFF_LOOPBACK) && (family == AF_INET) && getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) == 0)
        {
          if (!strcmp(message, ifa->ifa_name))
          {
            flag = 1;
            write(newsock, message, strlen(message));
            write(newsock, ifa->ifa_name, strlen(ifa->ifa_name));
            break ;
          }
        }
      }
    }
  }
  return (flag);
}

void kill_child(int sig)
{
    kill(child_pid, SIGTERM);
}

void sigquit_handler (int sig)
{
    assert(sig == SIGQUIT);
    pid_t self = getpid();
    if (parent_pid  != self)
      _exit(0);
}



#include "../inc/server.h"

int main(void)
{
  int status;
  int pid;

  if(geteuid() != 0)
  {
    printf("Start with sudo ./demon_server \n");
    return (0);
  }
  pid = fork();
  if (pid == -1)
    return (-1);
  else if (!pid)
  {
    umask(0);
    setsid();
    chdir("/");
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    while (1)
      status = demon_server();
    return status;
  }
  else
    return (0);
  return (0);
}

int  demon_server(void)
{
  int sock;
  fd_set socks;
  fd_set readsocks;
  int maxsock;
  int reuseaddr = 1;
  struct addrinfo hints, *res;
  struct sockaddr_in server , client;
  struct sockaddr_in their_addr;
  int newsock;
  socklen_t size;

  parent_pid = getpid();
  logfile=fopen("log.txt","w");
  signal(SIGQUIT, sigquit_handler);
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  if (getaddrinfo(NULL, PORT, &hints, &res) != 0) {
      perror("getaddrinfo");
      return (1);
  }
  sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
  if (sock == -1) {
      perror("socket");
      return (1);
  }
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int)) == -1) {
      perror("setsockopt");
      return (1);
  }
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons( 8888 );
  if( bind(sock ,(struct sockaddr *)&server , sizeof(server)) < 0)
  {
      perror("bind failed. Error");
      return (1);
  }
  freeaddrinfo(res);
  if (listen(sock, BACKLOG) == -1) {
      perror("listen");
      return 1;
  }
  FD_ZERO(&socks);
  FD_SET(sock, &socks);
  maxsock = sock;
  while (1) {
      unsigned int s;
      readsocks = socks;
      if (select(maxsock + 1, &readsocks, NULL, NULL, NULL) == -1) {
          perror("select");
          return (1);
      }
      for (s = 0; s <= maxsock; s++)
      {
        if (FD_ISSET(s, &readsocks))
        {
          if (s == sock)
          {
            size = sizeof(struct sockaddr_in);
            newsock = accept(sock, (struct sockaddr*)&their_addr, &size);
            if (newsock == -1)
            {
              perror("accept");
            }
            else
            {
              FD_SET(newsock, &socks);
              if (newsock > maxsock)
              {
                maxsock = newsock;
              }
            }
          }
          else
          {
            handle(s, &socks);
          }
        }
      }
  }
  close(sock);
  return (0);
}

int snifer(FILE *logfile, char *interface)
{
  int saddr_size , data_size;
  struct sockaddr saddr;
  struct in_addr in;
  char ip[255];
  unsigned char *buffer;

  signal(SIGKILL, kill_child);
  buffer = (unsigned char *)malloc(65536);
  if(logfile==NULL)
  {
    exit(0);
  }
  sock_raw = socket(AF_INET , SOCK_RAW , IPPROTO_TCP);
  if(sock_raw < 0)
  {
      return (1);
  }
  if (strcmp(interface, "all") != 0){
    if (setsockopt(sock_raw , SOL_SOCKET , SO_BINDTODEVICE ,interface , strlen(interface)+ 1 ) < 0)
    {
      return (0);
    }
  }
  while(1)
  {
    saddr_size = sizeof(saddr);
    data_size = recvfrom(sock_raw , buffer , 65536 , 0 , &saddr , &saddr_size);
    if(data_size <0)
    {
      return (1);
    }
    process_packet(buffer , data_size);
  }
  close(sock_raw);
  return (0);
}

void process_packet(unsigned char* buffer, int size)
{
    struct iphdr *iph = (struct iphdr*)buffer;
    ++total;
    switch (iph->protocol)
    {
        case 1:
            ++icmp;
            print_icmp_packet(buffer, size);
            break;

        case 2:
            ++igmp;
            break;

        case 6:
            ++tcp;
            print_tcp_packet(buffer , size);
            break;

        case 17:
            ++udp;
            print_udp_packet(buffer , size);
            break;

        default:
            ++others;
            break;
    }
}

void print_ip_header(unsigned char* Buffer, int Size)
{
    unsigned short iphdrlen;
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen =iph->ihl * 4;

    memset(&source, 0, sizeof(source));
    source.sin_addr.s_addr = iph->saddr;
    memset(&dest, 0, sizeof(dest));
    dest.sin_addr.s_addr = iph->daddr;
    fprintf(logfile,"   |-Source IP        : %s",inet_ntoa(source.sin_addr));
    fprintf(logfile,"   |-Destination IP   : %s",inet_ntoa(dest.sin_addr));
}

void print_tcp_packet(unsigned char* Buffer, int Size)
{
    unsigned short iphdrlen;
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen = iph->ihl*4;
    struct tcphdr *tcph=(struct tcphdr*)(Buffer + iphdrlen);
    fprintf(logfile,"TCP");
    fprintf(logfile, " %s ", asctime (timeinfo));
    print_ip_header(Buffer , Size);
    fprintf(logfile,"\n");
}

void print_udp_packet(unsigned char *Buffer , int Size)
{
    unsigned short iphdrlen;
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen = iph->ihl*4;
    struct udphdr *udph = (struct udphdr*)(Buffer + iphdrlen);
    fprintf(logfile,"UDP");
    fprintf(logfile, " %s ", asctime (timeinfo));
    print_ip_header(Buffer,Size);
    fprintf(logfile,"\n");
}

void print_icmp_packet(unsigned char* Buffer , int Size)
{
    unsigned short iphdrlen;
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    struct iphdr *iph = (struct iphdr *)Buffer;
    iphdrlen = iph->ihl*4;
    struct icmphdr *icmph = (struct icmphdr *)(Buffer + iphdrlen);
    fprintf(logfile,"ICMP");
    fprintf(logfile, " %s ", asctime (timeinfo));
    print_ip_header(Buffer , Size);
    fprintf(logfile,"\n");
}

void handle(int newsock, fd_set *set)
{
  char buff[100];
  int   start;
  int status;

  start = 0;
  memset(buff, 0, 100);
  read(newsock, buff, 99);
  if (!strcmp(buff, "start"))
  {
    start = 1;
    write(newsock, "\nStart snifing\n", strlen("\nStart snifing\n"));
    child_pid = fork();
    if (child_pid == 0)
    {
      snifer(logfile, "all");
    }
  }
  if (strcmp(buff, "exit") == 0)
  {
    close(newsock);
    kill(child_pid, SIGKILL);
    kill(parent_pid, SIGKILL);
    exit(0);
  }
  else if (strcmp(buff, "stop") == 0)
  {
    kill(-parent_pid, SIGQUIT);
    for (;;)
    {
      pid_t child = wait(&status);
      if (child > 0 && WIFEXITED(status) && WEXITSTATUS(status) == 0)
        ;
      else if (child < 0 && errno == EINTR)
        continue;
      else
      {
        perror("wait");
        abort();
      }
      break;
    }
  }
  else if (check_interface(buff, newsock) == 1)
  {
    kill(-parent_pid, SIGQUIT);
    for (;;)
    {
      pid_t child = wait(&status);
      if (child > 0 && WIFEXITED(status) && WEXITSTATUS(status) == 0)
            ;
      else if (child < 0 && errno == EINTR)
        continue;
      else
      {
        perror("wait");
        abort();
      }
      break;
    }
    write(newsock, "\nChange interface snifing\n", strlen("\nChange interface snifing\n"));
    child_pid = fork();
    if (child_pid == 0)
      snifer(logfile, buff);
  }
  write(newsock, "All Good", strlen("All Good"));
}

int   check_interface(char *message, int newsock)
{
  int   family;
  int   flag;
  char  *interface;
  struct ifaddrs *ifa;
  struct ifaddrs *ifaddr;
  char ip[255];

  flag = 0;
  if (getifaddrs(&ifaddr) == -1)
    return (0);
  else
  {
    i = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
      if (ifa->ifa_addr == NULL)
        continue;
      family = ifa->ifa_addr->sa_family;
      if (family == AF_INET || family == AF_INET6)
      {
        if(!(ifa->ifa_flags & IFF_LOOPBACK) && (family == AF_INET) && getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) == 0)
        {
          if (!strcmp(message, ifa->ifa_name))
          {
            flag = 1;
            write(newsock, message, strlen(message));
            write(newsock, ifa->ifa_name, strlen(ifa->ifa_name));
            break ;
          }
        }
      }
    }
  }
  return (flag);
}

void kill_child(int sig)
{
    kill(child_pid, SIGTERM);
}

void sigquit_handler (int sig)
{
    assert(sig == SIGQUIT);
    pid_t self = getpid();
    if (parent_pid  != self)
      _exit(0);
}
