
#include "../inc/client.h"

int main(int argc , char *argv[])
{
  int sock;
  struct sockaddr_in server;
  char *message, server_reply[2000];
  char *ptr;
	int ret;

  if(geteuid() != 0)
  {
    printf("Start with sudo ./demon_server \n");
    return (0);
  }
  sock = socket(AF_INET , SOCK_STREAM , 0);
  if (sock == -1)
  {
    printf("Could not create socket");
  }
  puts("Socket created");
  server.sin_addr.s_addr = inet_addr("127.0.0.1");
  server.sin_family = AF_INET;
  server.sin_port = htons(8888);
  if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
  {
    perror("connect failed. Error");
    return 1;
  }
  baner();
  about();
  while(1)
  {
		ret = 0;
    puts("Enter message : ");
    message = ft_getline();
    puts("\n");
		if(!strncmp(message, "stat", strlen("stat")))
		{
			ret = check_stat(message);
			if (ret)
				puts("ALL good\n");
			else
				puts("Check argument of stat\n");
		}
    if (!strcmp(message, "help"))
    {
      about();
    }
    else if(!strcmp(message, "exit"))
    {
      puts("Close client and snifer\n");
      if(send(sock , message , strlen(message) , 0) < 0)
	    {
	      puts("Send failed");
	      return (1);
	    }
      close(sock);
      exit(0);
    }
    else if(!strcmp(message, "stop"))
    {
      puts("Stoping snifer\n");
      if( send(sock , message , strlen(message) , 0) < 0)
      {
	      puts("Send failed");
	      return (1);
	    }
      if(recv(sock , server_reply , 2000 , 0) < 0)
	    {
	      puts("recv failed");
				return (1);
	    }
      puts("Server status :");
      puts(server_reply);
    }
    else if(!strcmp(message, "start"))
    {
      puts("Starting snifer\n");
      if(send(sock , message , strlen(message) , 0) < 0)
      {
	      puts("Send failed");
	      return (1);
	    }
      if(recv(sock , server_reply , 2000 , 0) < 0)
	    {
	      puts("recv failed");
	      return (1);
	    }
      puts("Server status :");
      puts(server_reply);
    }
    else if(strstr(message, "interface"))
    {
			ret = check_interface(message, sock);
      if (ret)
        ;
      else
	    {
	      puts("Check name of interface\n");
	      printf_name_of_interface();
	    }
    }
    else if(!strncmp(message, "show",strlen("show")))
    {
			ret = check_show(message);
      if (ret)
				puts("ALL good\n");
      else
      	puts("Check argument of show\n");
    }
    else if (chaeck_meassgen(message) == 0)
    {
      puts("Not corect query\n");
      puts("Use [help] for get help information\n");
    }
  }
  close(sock);
  return (0);
}

int   check_interface(char *message, int sock)
{
  int   i, j, family;
  int   flag;
  char  *interface;
  struct ifaddrs *ifa;
  struct ifaddrs *ifaddr;
  char ip[255];
  char server_reply[2000];
  char *tmp;

  i = strlen("interface") + 1;
  j = 0;
  flag = 0;
  if (!(interface = (char *)malloc(sizeof(char) * (strlen(message) - strlen("interface")))))
  {
    return (0);
  }
  while(message[i] != '\0')
  {
    interface[j] = message[i];
    i++;
    j++;
  }
  interface[j] = '\0';
  if (getifaddrs(&ifaddr) == -1)
  {
    return (0);
  }
  else
  {
    i = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
      if (ifa->ifa_addr == NULL)
      {
        continue;
      }
      family = ifa->ifa_addr->sa_family;
      if (family == AF_INET || family == AF_INET6)
      {
        if(!(ifa->ifa_flags & IFF_LOOPBACK) && (family == AF_INET) && getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) == 0)
        {
          if (!strncmp(interface, ifa->ifa_name, strlen(ifa->ifa_name)))
          {
						printf("%s and %s\n",interface,  ifa->ifa_name);
            flag = 1;
            break ;
          }
        }
      }
    }
  }
  if (flag)
  {
    freeifaddrs(ifaddr);
    if(send(sock , interface , strlen(interface) , 0) < 0)
    {
        puts("Send failed");
    }
    if( recv(sock , server_reply , 2000 , 0) < 0)
    {
        puts("recv failed");
        return (0);
    }
    puts("Server reply :");
    puts(server_reply);
    return (flag);
  }
  return (flag);
}

void printf_name_of_interface(void)
{
  int i, family;
  char  *interface;
  struct ifaddrs *ifa;
  struct ifaddrs *ifaddr;
  struct sockaddr saddr;
  char ip[255]; // ip interfac

  if (getifaddrs(&ifaddr) == -1)
  {
    return ;
  }
  else
  {
    i = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
      if (ifa->ifa_addr == NULL)
      {
        continue;
      }
      family = ifa->ifa_addr->sa_family;
      if (family == AF_INET || family == AF_INET6)
      {
        if(!(ifa->ifa_flags & IFF_LOOPBACK) && (family == AF_INET) && getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) == 0)
        {
          printf("Name: '%s'\n", ifa->ifa_name);
        }
      }
    }
  }
  freeifaddrs(ifaddr);
}

void  about(void)
{
  puts("Use -- start -- for start snifing\n");
  puts("Use -- stop -- for stop snifing\n");
  puts("Use -- interface [name] -- for select interface ");
  puts("or use -- interface -- for show all interface\n");
  puts("Use -- stat [name interface] -- for show all collected statistics for ");
  puts("particular interface or -- stat -- for show all statistics\n");
  puts("Use -- show [ip] -- print number of packets received from ip address ");
  puts("or -- show -- for all statistic\n");
  puts("Use -- exit -- for close snifir and client\n");
}

void  baner(void)
{
	puts(RED);
  puts(" ##   ##     #                 #     ##          #      #     #               \n");
  puts("#  #   #                       #    #  #               # #   # #              \n");
  puts("#      #    ##     ##   ###   ###    #    ###   ##     #     #     ##   ###   \n");
  puts("#      #     #    # ##  #  #   #      #   #  #   #    ###   ###   # ##  #  #  \n");
  puts("#  #   #     #    ##    #  #   #    #  #  #  #   #     #     #    ##    #     \n");
  puts(" ##   ###   ###    ##   #  #    ##   ##   #  #  ###    #     #     ##   # \n");
	puts(RESET);
}

int chaeck_meassgen(char *message)
{
  if(strcmp(message, "start") == 0)
    return (1);
  if(strcmp(message, "stop") == 0)
    return (1);
  if(strcmp(message, "exit") == 0)
    return (1);
  if(strcmp(message, "close") == 0)
    return (1);
  if (strncmp(message, "interface ", strlen("interface ")) == 0)
    return (1);
  if(strncmp(message, "stat", strlen("stat")) == 0)
      return (1);
  if(strncmp(message, "show", strlen("show")) == 0)
      return (1);
  if(strcmp(message, "help") == 0)
      return (1);
  return (0);
}

int   check_stat(char *message)
{
  int   i, j, family;
  char  *interface;
	char	*string;
  struct ifaddrs *ifa;
  struct ifaddrs *ifaddr;
  struct sockaddr saddr;
  struct in_addr in;
  char ip[255];

  i = strlen("stat") + 1;
  j = 0;
  if (!(interface = (char *)malloc(sizeof(char) * (strlen(message) - strlen("stat")))))
  {
    return (0);
  }
  while(message[i] != '\0')
  {
    interface[j] = message[i];
    i++;
    j++;
  }
  interface[j] = '\0';
	puts(interface);
  if (getifaddrs(&ifaddr) == -1)
  {
    return (0);
  }
  else
  {
    i = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
      if (ifa->ifa_addr == NULL)
      {
        continue;
      }
      family = ifa->ifa_addr->sa_family;
      if (family == AF_INET || family == AF_INET6)
      {
        if(!(ifa->ifa_flags & IFF_LOOPBACK) && (family == AF_INET) && getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) == 0)
        {
          if (!strncmp(interface, ifa->ifa_name, strlen(ifa->ifa_name)))
          {
						string = ReadFile("/log.txt");
						if (string)
						{
							printf("The %s inreface have %ld and ip of interface %s\n",interface,  substr_count(string, ip), ip);
							free(string);
							return (1);
						}
          }
        }
      }
    }
  }
  return (0);
}

int   check_show(char *message)
{
  int   i, j, flag;
  char  *adres;
	char	*string;
	int		count_number;

  i = strlen("show") + 1;
  j = 0;
  flag = 0;
	count_number = 0;
  if (!(adres = (char *)malloc(sizeof(char) * (strlen(message) - 9))))
  {
    return (0);
  }
  while(message[i] != '\0')
  {
    if ((message[i] >= '0' && message[i] <= '9') || message[i] == '.')
    {
      adres[j] = message[i];
      i++;
      j++;
    }
		else
		{
			printf("Check correct ip address\n");
			return (0);
		}
  }
  adres[j] = '\0';
  if (i != strlen(message))
  {
    printf("Check correct ip address\n");
    return (0);
  }
  if (flag = 1)
  {
		string = ReadFile("/log.txt");
		if (string)
		{
			printf("The %s ip have %ld\n",adres,  substr_count(string, adres));
			free(string);
			return (flag);
		}
  }
  return (flag);
}



size_t substr_count(const char *str, const char* substr)
{
    size_t count = 0;
    const char *p = str;
    do
    {
        p = strstr(p, substr);
    }
    while (p != NULL && ++count && *(++p) != '\0');
    return (count);
}

char* ReadFile(char *filename)
{
   char *buffer = NULL;
   int string_size, read_size;
   FILE *handler = fopen(filename, "r");

   if (handler)
   {
       fseek(handler, 0, SEEK_END);
       string_size = ftell(handler);
       rewind(handler);
       buffer = (char*) malloc(sizeof(char) * (string_size + 1) );
       read_size = fread(buffer, sizeof(char), string_size, handler);
       buffer[string_size] = '\0';
       if (string_size != read_size)
       {
           free(buffer);
           buffer = NULL;
       }
       fclose(handler);
    }
    return (buffer);
}

int		ft_getchar(void)
{
	char	c;

	if (read(0, &c, 1) != 1)
		return (-1);
	return ((int)c);
}

char	*ft_getline(void)
{
	int			i;
	int			c;
	static char	buff[BUFF_SIZE];

	i = 0;
	memset(buff, 0, BUFF_SIZE);
	while (1)
	{
		c = ft_getchar();
		if (c == '\n' || c == '\0')
		{
			buff[i] = '\0';
			return (buff);
		}
		else
			buff[i] = c;
		i++;
	}
	free(buff);
	return (NULL);
}
